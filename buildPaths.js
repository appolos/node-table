const path = require('path')

const buildPaths = {
	buildPathPdf: path.resolve('./build.pdf'),
	buildPathHtml: path.resolve('./build.html'),
}

module.exports = buildPaths
