# node-table

Simple Nodejs and Puppeteer table generator

## Base dependencies

- Node
- Headless Browser: Puppeteer

## Main scripts

Use these steps to install project

```
1. npm i
2.build:table: create html table,
3.build:pdf: create PDF
```
