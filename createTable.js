/* eslint-disable no-tabs */
const fs = require('fs')

// Initial JSON data
const data = require('./data.json')

// Build Paths
const { buildPathHtml } = require('./buildPaths')

/**
 * Take an object which has the following model
 * @param {Object} item
 * @model
 * {
 *   "invoiceId": `Number`,
 *   "createdDate": `String`,
 *   "dueDate": `String`,
 *   "address": `String`,
 *   "companyName": `String`,
 *   "invoiceName": `String`,
 *   "price": `Number`,
 * }
 *
 * @returns {String}
 */

const createRow = item => `
  <tr>
    <td>${item.invoiceId}</td>
    <td>${item.invoiceName}</td>
    <td>${item.price}</td>
    <td>${item.createdDate}</td>
    <td>${item.dueDate}</td>
    <td>${item.address}</td>
    <td>${item.companyName}</td>
  </tr>
 `
/**
 * @description Generates a html table with all the table rows
 * @param {String} rows
 * @returns {String}
 */

const createTable = rows => `
  <table>
    <tr>
        <th>Invoice Id</td>
        <th>Invoice Name</td>
        <th>Price</td>
        <th>Invoice Created</td>
        <th>Due Date</td>
        <th>Vendor Address</td>
        <th>Vendor Name</td>
    </tr>
    ${rows}
  </table>
 `
/**
 * @description Generate an `html` page with a populated table
 * @param {String} table
 * @returns {String}
 */
const createHtml = table => `
  <html>
    <head>
      <style>
        table {
          width: 100%;
        }
        tr {
          text-align: left;
          border: 1px solid black;
        }
        th, td {
          padding: 15px;
        }
        tr:nth-child(odd) {
          background: #CCC
        }
        tr:nth-child(even) {
          background: #FFF
        }
        .no-content {
          background-color: red;
        }
      </style>
    </head>
    <body>
      ${table}
    </body>
  </html>
`


/**
 * @description this method takes in a path as a string & returns true/false
 * as to if the specified file path exists in the system or not.
 * @param {String} filePath
 * @returns {Boolean}
 */

const isFileExist = (filePath) => {
	try {
		fs.statSync(filePath)
		return true
	} catch (error) {
		return false
	}
}


try {
	/** Check if file exist for html build exists in system or not  */
	if (isFileExist(buildPathHtml)) {
		console.log('Deleting old build file')
		fs.unlinkSync(buildPathHtml)
	}
	/* Generate rows */
	const rows = data.map(createRow).join('')
	/* Generate table */
	const table = createTable(rows)
	/* Generate html */
	const html = createHtml(table)

	/* Write the generated html to file */
	fs.writeFileSync(buildPathHtml, html)
	console.log('Successfully created an HTML table')
} catch (error) {
	console.log('Error generating table', error);
}
